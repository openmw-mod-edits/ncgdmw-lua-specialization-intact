all: ncgdmw.omwaddon ncgdmw_alt_start.omwaddon ncgdmw_starwind.omwaddon

clean:
	rm -f *.zip *.sha*sum.txt version.txt

clean-omwaddons:
	rm -f *.omwaddon

ncgdmw.omwaddon:
	./build.sh ncgdmw

ncgdmw_alt_start.omwaddon:
	./build.sh ncgdmw_alt_start

ncgdmw_starwind.omwaddon:
	./build.sh ncgdmw_starwind

pkg:
	./build.sh

web-clean:
	cd web && rm -rf build site/*.md sha256sums*

web: web-clean
	cd web && ./build.sh

web-debug: web-clean
	cd web && ./build.sh --debug

web-verbose: web-clean
	cd web && ./build.sh --verbose

clean-all: clean web-clean clean-omwaddons

local-web:
	cd web && python3 -m http.server -d build
